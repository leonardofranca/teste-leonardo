// Carrega os filmes que estao em cartaz no cinema
function carregaFilmesCinema() {
    $.get(config.URL_API+'movie/now_playing?api_key='+config.API_KEY+'&language='+config.LANG_API, function( data ) {
        var conteudo = '';
        // Monta o html dos filmes em cartaz
        for (let i = 0; i < 3; i++) {
            conteudo += '<article class="item-filme">' +
                            '<a class="item-filme" href="javascript:carregaModalFilme('+data.results[i].id+')" title="Clique para ver mais detalhes" style="background-image: url(https://image.tmdb.org/t/p/w500'+data.results[i].poster_path+')">' +
                            '</a>' +
                        '</article>';
        }
        document.getElementById("carousel-filmes-cartaz").innerHTML = conteudo;

        // Carrega o carousel de filmes em cartaz
        $('#carousel-filmes-cartaz').owlCarousel({
            loop: true,
            nav: true,
            height: '50px',
            margin: 10,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2
                },
                768: {
                    items: 3
                }
            }
        });
    });
}
// Realiza a pesquisa e faz o auto complete
function realizaPesquisa(campo) {
    $(campo).keyup(function () {
        $(campo).autocomplete({
            source: function (request, response) {
                $.get(config.URL_API+"search/movie?api_key="+config.API_KEY+"&language="+config.LANG_API+"&query=" + $(campo).val(), function (data) {
                    response($.map(data.results, function (value, key) {
                        var dataLancamento;
                        // Testa se existe a data de lançamento, se não tive coloca como indefinido
                        if (value.release_date !== '') {
                            dataLancamento = value.release_date.split("-");
                            dataLancamento = dataLancamento[2]+'/'+dataLancamento[1]+'/'+dataLancamento[0];
                        } else {
                            dataLancamento = 'Não definido';
                        }

                        return {
                            value: value.title+' ('+dataLancamento+')',
                            url: 'javascript:carregaModalFilme('+value.id+')'
                        };
                    }));
                });
            },
            select: function( event, ui ) {
                window.location.href = ui.item.url;
            },
            minLength: 1,
            delay: 100
        });
    });
}

function carregaModalFilme(id) {
    // Caso o id da url nao for passado, ele redireciona para a pagina inicial
    if(id) {
        // Faz a requisicao para api com o id do filme que foi passado por url
        $.get(config.URL_API+'movie/'+id+'?api_key='+config.API_KEY+'&language='+config.LANG_API+'&append_to_response=credits,videos', function( data ) {
            var conteudo = '',
                dataLancamento;
            // Testa se existe a data de lançamento, se não tive coloca como indefinido
            if (data.release_date !== '') {
                dataLancamento = data.release_date.split("-");
                dataLancamento = dataLancamento[2]+'/'+dataLancamento[1]+'/'+dataLancamento[0];
            } else {
                dataLancamento = 'Não definido';
            }

            var generos = '';
            if (data.genres.length != 0) {
                for (var genero in data.genres) {
                    generos += (genero > 0 ? ', ' : '');
                    generos += data.genres[genero].name;
                }
            } else {
                generos = 'Sem Gênero';
            }

            var atores = '';

            if (data.credits.cast.length != 0) {
                for (var elenco in data.credits.cast) {
                    atores += (elenco > 0 ? ', ' : '');
                    atores += data.credits.cast[elenco].name;
                    if (elenco >= 4) {
                        break;
                    }
                }
            } else {
                atores = 'Indefinido';
            }

            // monta o html da descricao do filme
            conteudo += '<div class="card">\n' +
                            '<div class="row no-gutters filme">' +
                                '<div class="col-sm-4">' +
                                    '<img src="'+(data.poster_path != null ? "https://image.tmdb.org/t/p/w500"+data.poster_path : "imagens/sem_imagem.jpg") +'" class="card-img" alt="'+data.title+'">' +
                                '</div>' +
                                '<div class="col-sm-8" style="background-image: url('+(data.backdrop_path != null ? "https://image.tmdb.org/t/p/original"+data.backdrop_path : "imagens/sem_imagem.jpg")+'); background-size: cover; ">' +
                                    '<div class="card-body">' +
                                        '<h5 class="card-title"><i class="fas fa-film"></i>  '+data.title+'</h5>' +
                                        '<p class="card-text sinopse">'+(data.overview !== '' ? data.overview : 'Sem Sinopse') +'</p>' +
                                        '<p class="card-text nota"><span class="ratecinema"></span><i class="fas fa-thumbs-up"></i>Nota: '+(data.vote_average/2).toFixed(1)+'' +
                                            (data.videos.results.length != 0 ?
                                            '<a data-fancybox href="https://www.youtube.com/watch?v='+data.videos.results[0].key+'"><i class="fas fa-play"></i>Trailer</a>'
                                                : '')+
                                        '</p>' +
                                        '<p class="card-text">Gênero(s) : '+generos+'</p>' +
                                        '<p class="card-text">Atores : '+atores+'</p>' +
                                        '<p class="card-text">Data de Lançamento : '+dataLancamento+'</p>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            // adiciona o html a div descricao
            document.getElementById('desc-filme').innerHTML = conteudo;

            // monta a avaliacao do filme utilizando o framework Rateyo
            $(function () {
                var nota = data.vote_average/2;
                $(".ratecinema").rateYo({
                    rating: nota,
                    readOnly: true,
                    numStars: 5,
                    starWidth: "22px",
                    ratedFill: "#ff6666",
                    spacing: "7px"
                });
            });

            $("#modal-filme").modal();
        });
    } else {
        location.href="index.html";
    }
}